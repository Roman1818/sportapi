<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sport Life</title>
</head>
<body>
<h1 align="center">Sport Life</h1>
<br/>
<table border="3">
    <tr>
        <td>Name of service</td>
        <td>Example of request</td>
    </tr>
    <tr>
        <td>Add user</td>
        <td>http://localhost:8080/SportAPI_war_exploded/add ({"name":"'your name'"})</td>
    </tr>
    <tr>
        <td>Get Sub</td>
        <td>http://localhost:8080/SportAPI_war_exploded/getSub</td>
    </tr>
    <tr>
        <td>Buy</td>
        <td>http://localhost:8080/SportAPI_war_exploded/buy?name='your name'&id='id of subscription'</td>
    </tr>
    <tr>
        <td>Get Time</td>
        <td>http://localhost:8080/SportAPI_war_exploded/getTime?name='your name'</td>
    </tr>
    <tr>
        <td>Enter</td>
        <td>http://localhost:8080/SportAPI_war_exploded/enter?name='your name'</td>
    </tr>
    <tr>
        <td>Exit</td>
        <td>http://localhost:8080/SportAPI_war_exploded/exit?name='your name'</td>
    </tr>




</table>
<h3></h3>
</body>
</html>
