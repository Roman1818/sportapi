package org.project.sportAPI;


import org.project.sportAPI.dao.SportDAO;
import org.project.sportAPI.dao.SportDBDAO;
import org.project.sportAPI.service.SportService;
import org.project.sportAPI.service.SportServiceImpl;
import org.project.sportAPI.setup.SetUpDb;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.sql.DataSource;

@Configuration
public class AppConfiguration {



    @Bean
    ViewResolver viewResolver(){
        InternalResourceViewResolver ivr = new InternalResourceViewResolver();
        ivr.setPrefix("/WEB-INF/views/");
        ivr.setSuffix(".jsp");
        return ivr;
    }



    @Bean
    public DataSource dataSource(){
        SetUpDb setUpDb = new SetUpDb();
        return setUpDb.getDataSource();

    }

    @Bean
    public SportDAO sportDAO(){
        return new SportDBDAO(dataSource());
    }


    @Bean
    public SportService sportService(){
        return new SportServiceImpl(sportDAO());
    }


}
