package org.project.sportAPI.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuController {


    @GetMapping("")
    String index() {
        return "menu";
    }
}
