package org.project.sportAPI.controller;

import org.project.sportAPI.dto.Customer;
import org.project.sportAPI.dto.Subscription;
import org.project.sportAPI.service.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MainController {

    private final SportService sportService;

    @Autowired
    public MainController(SportService sportService) {
        this.sportService = sportService;
    }




    @PostMapping("add")
    Customer add(@RequestBody Customer customer) {
        sportService.register(customer.getName());
        return customer;
    }

    @GetMapping("getSub")
    List<Subscription> getSub() {
        return sportService.getSub();


    }

    @GetMapping("buy")
    String buy(@RequestParam String name, Integer id) {
        if (name == null || id == null) return "Enter data!";
        return sportService.buy(name, id);
    }

    @GetMapping("getTime")
    String getTime(@RequestParam String name){
        if(name==null) return "Enter data!";
        return sportService.getTime(name);
    }

    @GetMapping("enter")
    String enter(@RequestParam String name){
        return  sportService.enter(name);
    }

    @GetMapping("exit")
    String exit(@RequestParam String name){
        return sportService.exit(name);
    }


}
