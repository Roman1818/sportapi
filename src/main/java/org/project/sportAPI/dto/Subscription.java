package org.project.sportAPI.dto;

import lombok.Data;
import lombok.experimental.Accessors;



@Data
@Accessors(chain = true)
public class Subscription {
    private Integer id;
    private String name;
    private Long time;
    private Integer price;


}
