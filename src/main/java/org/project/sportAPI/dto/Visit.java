package org.project.sportAPI.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class Visit {
    private Integer id;
    private LocalDateTime start;
    private LocalDateTime finish;
    private Long wholeTime;
    private Integer subscription_id;
}
