package org.project.sportAPI.service;

import lombok.RequiredArgsConstructor;
import org.project.sportAPI.dao.SportDAO;
import org.project.sportAPI.dto.CustomerSubscription;
import org.project.sportAPI.dto.Subscription;
import org.project.sportAPI.dto.Visit;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@RequiredArgsConstructor
public class SportServiceImpl implements SportService{

    private final SportDAO sportDAO;

    @Override
    public void register(String name) {
        LocalDate localDate = LocalDate.now();
        sportDAO.add(name, localDate);
    }

    @Override
    public List<Subscription> getSub() {
        return sportDAO.getAvailableSub();
    }


    @Override
    public String buy(String name, Integer sub_id) {
        if(getSub().stream().noneMatch(s -> s.getId().equals(sub_id))) return "This subscription has already been purchased!";
        CustomerSubscription cs = sportDAO.getCS(name);
        if (cs == null){
            sportDAO.creatCustomerSub(sub_id, name);
            return "Congratulations on your purchase!";
        }else{
            if(cs.getStatus()){
                return "You already have a subscription!";
            }else{
                sportDAO.updateCS(sub_id, name, true);
                return "Your subscription has been updated!";
            }
        }

    }

    @Override
    public String getTime(String name) {
        return convertTime(sportDAO.getTime(name));
    }

    @Override
    public String enter(String name) {
        Long time = sportDAO.getTime(name);
        if (time <= 0) return "You have no time in subscription!";
        if (sportDAO.checkExistVisit(name)) return "You are already in gym!";
        sportDAO.creatNewVisit(name);
        return "Good luck workout!";
    }

    @Override
    public String exit(String name) {
        if (!sportDAO.checkExistVisit(name)) return "You are not in the gym!";
        Visit visit = sportDAO.getVisit(name);
        visit.setFinish(LocalDateTime.now());
        visit.setWholeTime(Duration.between(visit.getStart(), visit.getFinish()).getSeconds());
        sportDAO.updateVisit(visit);
        Long time = sportDAO.getTime(name) - visit.getWholeTime();
        if (time <= 0){
            sportDAO.deleteSub(visit.getSubscription_id());
            sportDAO.updateCS(null, name, false);
            return "Your subscription has been deprecated!";
        }
        sportDAO.updateSub(visit.getSubscription_id(), time);
        return "We will be waiting for you!";
    }

    private String convertTime(Long seconds){
        Long hours =  seconds / 3600;
        Long remainder =  seconds - hours * 3600;
        Long mins = remainder / 60;
        remainder = remainder - mins * 60;
        Long secs = remainder;
        return String.format("%d:%02d:%02d", hours, mins, secs);
    }
}
