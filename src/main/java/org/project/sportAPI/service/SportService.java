package org.project.sportAPI.service;


import org.project.sportAPI.dto.Subscription;

import java.util.List;

public interface SportService {

    void register(String name);
    List<Subscription> getSub();
    String buy(String name, Integer id);
    String getTime(String name);
    String enter(String name);
    String exit(String name);
}
