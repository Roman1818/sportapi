package org.project.sportAPI.setup;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class SetUpDb {

    private final String dsn = "jdbc:postgresql://localhost:8000/sportlife";
    private final String user = "postgres";
    private final String password = "postgres";
    private DataSource ds;

    public SetUpDb() {
        ds = new HikariDataSource(createPool());

    }
    private HikariConfig createPool() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(org.postgresql.Driver.class.getName());
        hikariConfig.setJdbcUrl(dsn);
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(password);
        hikariConfig.setMaximumPoolSize(4);
        hikariConfig.setMinimumIdle(4);
        return new HikariDataSource(hikariConfig);
    }

    public DataSource getDataSource(){
        return ds;
    }
}
