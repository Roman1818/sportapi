package org.project.sportAPI.dao;


import org.project.sportAPI.dto.CustomerSubscription;
import org.project.sportAPI.dto.Subscription;
import org.project.sportAPI.dto.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class SportDBDAO implements SportDAO {

    private final DataSource dataSource;

    @Autowired
    public SportDBDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void add(String name, LocalDate localDate) {
        try (Connection connection = dataSource.getConnection();) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO customer(name, reg_time) VALUES (?, ?)"
            );
            ps.setString(1, name);
            ps.setObject(2, localDate);
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public List<Subscription> getAvailableSub() {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM subscription WHERE NOT EXISTS(" +
                            "    SELECT FROM customer_subscription WHERE subscription_id=subscription.id" +
                            ")"
            );
            ResultSet rs = ps.executeQuery();
            List<Subscription> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Subscription()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .setTime(rs.getLong("time"))
                        .setPrice(rs.getInt("price"))
                );
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public CustomerSubscription getCS(String name) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT customer_subscription.id, subscription_id, customer_id, buy_date, status " +
                            "FROM customer_subscription LEFT JOIN customer on " +
                            "customer.id = customer_subscription.customer_id WHERE customer.name=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new CustomerSubscription()
                        .setId(rs.getInt("id"))
                        .setCustomer_id(rs.getInt("customer_id"))
                        .setSubscription_id(rs.getInt("subscription_id"))
                        .setBuyDate(rs.getObject("buy_date", LocalDate.class))
                        .setStatus(rs.getBoolean("status"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public Long getTime(String name) {
        Integer cus_id = getIdCustomer(name);
            try (Connection connection = dataSource.getConnection()) {
                if (cus_id == null) throw new SQLException("Customer not exist!");
                PreparedStatement ps = connection.prepareStatement(
                        "SELECT time FROM subscription INNER JOIN customer_subscription " +
                                "on subscription.id = customer_subscription.subscription_id WHERE customer_id=?"
                );
                ps.setInt(1, cus_id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    return rs.getLong("time");
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        return null;
    }

    public Visit getVisit(String name){
        Integer sub_id = getIdSub(getIdCustomer(name));
        try (Connection connection = dataSource.getConnection()){
            if (sub_id == null) throw new SQLException("Customer or subscription not exist!");
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT id, start, finish, whole_time, subscription_id " +
                            "FROM visit WHERE subscription_id=?"
            );
            ps.setInt(1, sub_id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return new Visit()
                        .setId(rs.getInt("id"))
                        .setStart(rs.getObject("start", LocalDateTime.class))
                        .setWholeTime(rs.getLong("whole_time"))
                        .setSubscription_id(rs.getInt("subscription_id"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    private Integer getIdCustomer(String name) {
        try (Connection connection = dataSource.getConnection();) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT id FROM customer WHERE name=?"
            );
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


    private Integer getIdSub(Integer cus_id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT subscription.id FROM subscription LEFT JOIN customer_subscription " +
                            "on customer_subscription.subscription_id = subscription.id WHERE customer_id=?"
            );
            ps.setInt(1, cus_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

        return null;
    }


    @Override
    public void updateCS(Integer sub_id, String name, Boolean status) {
        Integer cus_id = getIdCustomer(name);
        try (Connection connection = dataSource.getConnection()) {
            if (cus_id == null) throw new SQLException("Customer not exist!");
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE customer_subscription SET " +
                            "status=?, subscription_id=?, buy_date=?  WHERE customer_id=?"
            );
            ps.setBoolean(1, status);
            ps.setInt(2, sub_id);
            ps.setObject(3, LocalDate.now());
            ps.setInt(4, cus_id);
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void updateVisit(Visit visit){
        try (Connection connection = dataSource.getConnection()) {
            if (visit == null) throw new SQLException("Problem with server!");
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE visit SET finish=?, whole_time=?  WHERE id=?"
            );
            ps.setObject(1, visit.getFinish());
            ps.setLong(2, visit.getWholeTime());
            ps.setInt(3, visit.getId());
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void updateSub(Integer id, Long time) {
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE subscription SET time=? WHERE id=?"
            );
            ps.setLong(1, time);
            ps.setInt(2, id);
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
    }







    @Override
    public boolean checkExistVisit(String name) {
        Integer sub_id = getIdSub(getIdCustomer(name));
            try (Connection connection = dataSource.getConnection()) {
                if (sub_id == null) throw new SQLException("Customer or subscription not exist!");
                PreparedStatement ps = connection.prepareStatement(
                        "SELECT id FROM visit WHERE subscription_id=?"
                );
                ps.setInt(1, sub_id);
                ResultSet rs = ps.executeQuery();
                return rs.next();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public void creatNewVisit(String name) {
        LocalDateTime localDateTime = LocalDateTime.now();
        Integer sub_id = getIdSub(getIdCustomer(name));
        try (Connection connection = dataSource.getConnection()) {
            if (sub_id == null) throw new SQLException("Customer or subscription not exist!");
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO visit(start, subscription_id) VALUES (?, ?)"
            );
            ps.setObject(1, localDateTime);
            ps.setInt(2, sub_id);
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    @Override
    public void creatCustomerSub(Integer sub_id, String name) {
        Integer cus_id = getIdCustomer(name);
        LocalDate ld = LocalDate.now();
        try {
            if (cus_id == null) throw new SQLException("Customer not exist!");
            Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO customer_subscription(subscription_id, customer_id, buy_date, status) VALUES (?, ?, ?, ?)"
            );
            ps.setInt(1, sub_id);
            ps.setInt(2, cus_id);
            ps.setObject(3, ld);
            ps.setBoolean(4, true);
            ps.execute();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


    }


    @Override
    public void deleteSub(Integer id) {
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement ps = connection.prepareStatement(
                    "DELETE FROM subscription WHERE id=?"
            );
            ps.setInt(1, id);
            ps.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
    }




}
