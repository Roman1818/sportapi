package org.project.sportAPI.dao;

import org.project.sportAPI.dto.CustomerSubscription;
import org.project.sportAPI.dto.Subscription;
import org.project.sportAPI.dto.Visit;

import java.time.LocalDate;
import java.util.List;

public interface SportDAO {


    void add(String name, LocalDate localDate);
    CustomerSubscription getCS(String name);
    Long getTime(String name);
    List<Subscription> getAvailableSub();
    Visit getVisit(String name);

    void updateCS(Integer sub_id, String name, Boolean status);
    void updateVisit(Visit visit);
    void updateSub(Integer id, Long time);

    boolean checkExistVisit(String name);

    void creatNewVisit(String name);
    void creatCustomerSub(Integer sub_id, String name);

    void deleteSub(Integer id);


}
